package org.opentele.server.lab

import dk.sosi.seal.model.Request
import dk.sosi.seal.xml.XmlUtil
import groovy.util.slurpersupport.GPathResult
import groovy.xml.MarkupBuilder
import org.opentele.server.lab.pogo.Emessage
import org.opentele.server.model.Clinician
import org.opentele.server.model.Patient
import org.springframework.beans.factory.annotation.Value
import org.w3c.dom.Document
import org.xml.sax.InputSource
import wslite.soap.SOAPClient
import wslite.soap.SOAPResponse

import javax.xml.parsers.DocumentBuilder
import javax.xml.parsers.DocumentBuilderFactory
import java.text.SimpleDateFormat

@SuppressWarnings("GrMethodMayBeStatic")
class LabPortalService {

    // --*-- Fields --*--

    private static final String REQUEST_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'"
    private static final String TIME_ZONE = 'UTC+01:00'

    private static final String SOAP_ADDRESSING_NAMESPACE = 'http://schemas.xmlsoap.org/ws/2004/08/addressing'

    private static final Map<String, String> SUPPORTED_MESSAGE_NAMESPACES = [
            'http://rep.oio.dk/sundcom.dk/medcom.dk/xml/schemas/2007/02/01/': 'XRPT01'
    ]
    private static final Map<String, String> UNSUPPORTED_MESSAGE_NAMESPACES = [
            'http://rep.oio.dk/sundcom.dk/medcom.dk/xml/schemas/2005/08/07/': 'XRPT04'
    ]

    private static final String LABSVAR_NAMESPACE = "http://laboratoriemedicinsk-svarindsamler.dsdn.dk"
    private static final String SOAP_ACTION = '"http://laboratoriemedicinsk-svarindsamler.dsdn.dk#HentSvarFraAlle"'

    @Value('${lab.username:""}')
    String username

    @Value('${lab.password:""}')
    String password

    @Value('${lab.organization:""}')
    String endUserOrganization

    @Value('${lab.url:""}')
    String url

    @Value('${lab.npuCodes:""}')
    String npuCodesString

    def sosiService
    def xrpt01ParserService
    def labResultCachingService
    def grailsApplication
    List npuCodes

    // --*-- Methods --*--

    List<Emessage> sendRequest(Clinician clinician, Patient patient, Date from,
                               Date to, int userTimeoutInSeconds) {
        npuCodes = npuCodesString.split(',')
        // When testing: ignore this statement..
        List<Emessage> result = labResultCachingService.getCachedResult(patient)
        // ...and replace with List<Emessage> result = []

        if (result.isEmpty()) {
            result = send(clinician, patient, from, to, userTimeoutInSeconds)
            labResultCachingService.setCachedResult(patient, result)
        }
        result
    }

    private List<Emessage> send(Clinician clinician, Patient patient, Date from,
                                Date to, int userTimeoutInSeconds) {

        Request sosiRequest = createSosiRequest()
        addSoapBody(clinician, patient, from, to, userTimeoutInSeconds, sosiRequest)
        addSoapAddressingHeaders(sosiRequest)

        // Ignore this block when testing...
        String soapRequest = XmlUtil.node2String(sosiRequest.serialize2DOMDocument(), false, true)
        SOAPClient soapClient = new SOAPClient(url)
        SOAPResponse response = soapClient.send(soapRequest)
        // ...and replace with: def response = new File('web-app/LabPortalResponseExample.xml')

        List<Emessage> eMessages = parseLabPortalResponse(response.text)
        eMessages
    }

    private Request createSosiRequest() {
        if (!username) {
            throw new Exception("lab.errors.input.missingUsername")
        }
        if (!password) {
            throw new Exception("lab.errors.input.missingPassword")
        }

        String alternativeIdentifier = "OpenTele"
        def sosiRequest = sosiService.createUnsignedRequest(username, password,
                alternativeIdentifier)
        sosiRequest
    }

    private void addSoapBody(Clinician clinician, Patient patient, Date from,
                             Date to, int userTimeoutInSeconds, Request sosiRequest) {

        if (!endUserOrganization) {
            throw new Exception("lab.errors.input.missingOrganization")
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat(REQUEST_DATE_FORMAT)
        dateFormat.setTimeZone(TimeZone.getTimeZone(TIME_ZONE))

        String cpr = patient.cpr
        String fromDate = dateFormat.format(from)
        String toDate = dateFormat.format(to)
        String endUserId = clinician.name
        int consentGiven = 1
        String consentText = ""

        def xmlWriter = new StringWriter()
        def xmlBuilder = new MarkupBuilder(xmlWriter)
        xmlBuilder.doubleQuotes = true
        xmlBuilder.HentSvarFraAlle(xmlns: "${LABSVAR_NAMESPACE}") {
            patientCpr(cpr)
            fraDato(fromDate.toString())
            tilDato(toDate.toString())
            slutBrugerIdentifikation(endUserId)
            slutBrugerOrganisation(endUserOrganization)
            samtykkeKode(consentGiven)
            samtykkeTekst(consentText)
            brugerTimeoutSekunder(userTimeoutInSeconds)
        }

        Document doc = createDocument(xmlWriter)
        sosiRequest.setBody(doc.getDocumentElement())
    }

    private Document createDocument(StringWriter xmlWriter) {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance()
        dbf.setNamespaceAware(true)
        DocumentBuilder db = dbf.newDocumentBuilder()
        Document doc = db.parse(new InputSource(new StringReader(xmlWriter.toString())))
        doc
    }

    private void addSoapAddressingHeaders(sosiRequest) {
        if (!url) {
            throw new Exception("lab.errors.input.missingUrl")
        }

        List<String> soapHeaders = [
                "<Action xmlns=\"${SOAP_ADDRESSING_NAMESPACE}\">${SOAP_ACTION}</Action>",
                "<To xmlns=\"${SOAP_ADDRESSING_NAMESPACE}\">${url}</To>"
        ]

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance()
        dbf.setNamespaceAware(true)
        DocumentBuilder db = dbf.newDocumentBuilder()

        soapHeaders.each { header ->
            Document headerDoc = db.parse(new InputSource(new StringReader(header)))
            sosiRequest.addNonSOSIHeader(headerDoc.getDocumentElement())
        }
    }

    List<Emessage> parseLabPortalResponse(String response) {

        def messages = []
        GPathResult xmlResponse = new XmlSlurper().parseText(response)

        def eMessageXmlWrappers = extractEmessageWrappers(xmlResponse)
        eMessageXmlWrappers.each { eMessageWrapper ->

            def messageStatus = checkEmessageWrapper(eMessageWrapper)
            if (messageStatus.isValid) {
                def eMessageXml = eMessageWrapper.LeverandoerSvarXml
                def eMessageXmlText = eMessageXml.text()
                messages << xrpt01ParserService.parse(eMessageXmlText as String, npuCodes)
            } else {
                log.error("Unable to parse message from '${messageStatus.errorDescription.supplier}'. " +
                        "Reason: ${messageStatus.errorDescription.message}")
            }
        }

        messages
    }

    private def extractEmessageWrappers(GPathResult xmlResponse) {

        if (findChildrenWithName(xmlResponse, 'Header').size() == 0) {
            throw new Exception("lab.errors.output.missingHeader")
        }

        if (findChildrenWithName(xmlResponse, 'Body').size() == 0) {
            throw new Exception("lab.errors.output.missingBody")
        }
        def body = xmlResponse.Body

        if (findChildrenWithName(body, 'HentSvarFraAlleResponse').size() == 0) {
            def fault = body.Fault
            def faultCode = fault.faultcode
            def faultString = fault.faultstring
            throw new Exception("${faultCode}:${faultString}")
        }
        def hentSvarFraAlleResponse = body.HentSvarFraAlleResponse

        if (findChildrenWithName(hentSvarFraAlleResponse, 'HentSvarFraAlleResult').size() == 0) {
            throw new Exception("lab.errors.output.missingHentSvarFraAlleResult")
        }
        def hentSvarFraAlleResult = hentSvarFraAlleResponse.HentSvarFraAlleResult

        if (findChildrenWithName(hentSvarFraAlleResult, 'LeverandoerSvarDtoListe').size() == 0) {
            throw new Exception("lab.errors.output.missingLeverandoerSvarDtoListe")
        }
        def leverandoerSvarDtoListe = hentSvarFraAlleResult.LeverandoerSvarDtoListe

        if (findChildrenWithName(leverandoerSvarDtoListe, 'LeverandoerSvarDto').size() == 0) {
            throw new Exception("lab.errors.output.missingLeverandoerSvarDto")
        }
        def leverandoerSvarDTo = findChildrenWithName(leverandoerSvarDtoListe, 'LeverandoerSvarDto')

        leverandoerSvarDTo
    }

    private def findChildrenWithName(parent, String childName) {
        parent.children().findAll({ it.name() == childName })
    }

    private Map checkEmessageWrapper(eMessage) {

        def result = [:]
        String status = eMessage.Status.text()
        String namespace = eMessage.Namespace.text()
        String supplier = eMessage.LeverandoerNavn.text()

        if (status == 'Ok') {
            def messageType = SUPPORTED_MESSAGE_NAMESPACES[namespace]
            if (!messageType) {
                def unsupportedType = UNSUPPORTED_MESSAGE_NAMESPACES.containsKey(namespace) ?
                        UNSUPPORTED_MESSAGE_NAMESPACES[namespace] : namespace
                result.isValid = false
                result.errorDescription = [
                        supplier: supplier,
                        message: "Message of type $unsupportedType is not supported"
                ]
                return result
            }
            result.isValid = true
        } else {
            result.isValid = false
            result.errorDescription = [
                    supplier: supplier,
                    message: "Received error response $status"
            ]
        }

        result
    }
}