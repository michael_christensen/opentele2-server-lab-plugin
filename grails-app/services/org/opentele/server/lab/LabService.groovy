package org.opentele.server.lab

import org.opentele.server.core.util.NumberFormatUtil
import org.opentele.server.lab.measurements.*
import org.opentele.server.lab.pogo.*
import org.opentele.server.model.Clinician
import org.opentele.server.model.Patient
import org.opentele.server.model.User
import org.springframework.beans.factory.annotation.Value

import java.text.SimpleDateFormat

@SuppressWarnings("GrMethodMayBeStatic")
class LabService {

    // --*-- Fields --*--

    private static final String RESULTS_DATE_FORMAT = "yyyy-MM-dd HH:mm"
    private static final String UNSPECIFIED = "uspecificeret"

    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(RESULTS_DATE_FORMAT)

    @Value('${lab.intervalInDays:90}')
    int intervalInDays

    @Value('${lab.timeoutInSeconds:30}')
    int timeoutInSeconds

    @Value('${lab.nknTablePropertiesPath:""}')
    String nknTablePropertiesPath

    def springSecurityService
    def labPortalService

    // --*-- Methods --*--

    List<MeasurementGroup> getMeasurementGroups(Patient patient) {
        List<Emessage> eMessages = sendRequest(patient)

        List<LabMeasurement> labMeasurements = collectLabMeasurements(eMessages)
        List<MeasurementGroup> measurementGroups = arrangeByMeasurementGroup(labMeasurements)

        measurementGroups
    }

    private List<Emessage> sendRequest(Patient patient) {
        def clinician = currentClinician()
        def (Date from, Date to) = getFromAndToDates(intervalInDays)
        List<Emessage> eMessages = labPortalService.sendRequest(clinician,
                patient, from, to, timeoutInSeconds)
        eMessages
    }

    private Clinician currentClinician() {
        Clinician.findByUser(springSecurityService.currentUser as User)
    }

    private List getFromAndToDates(int intervalInDays) {
        Date to = new Date()
        Calendar fromCalendar = new GregorianCalendar()
        fromCalendar.add(Calendar.DAY_OF_YEAR, -intervalInDays)
        Date from = fromCalendar.getTime()
        [from, to]
    }

    private List collectLabMeasurements(List<Emessage> eMessages) {
        def laboratoryReports = (eMessages.collect {
            it.laboratoryReports
        }).flatten()
        def labMeasurements = []
        laboratoryReports.each { laboratoryReport ->

            def consentGiven = laboratoryReport.patient.consentGiven ?
                    Boolean.valueOf(laboratoryReport.patient.consentGiven as String) :
                    true
            if (!consentGiven) {
                log.error "Consent not given!"
                return
            }

            def laboratoryResults = laboratoryReport.laboratoryResults
            labMeasurements.addAll(extractLabMeasurements(laboratoryResults))
        }
        labMeasurements
    }

    @SuppressWarnings("GroovyAssignabilityCheck")
    private List<LabMeasurement> extractLabMeasurements(LaboratoryResults laboratoryResults) {
        Date date = parseResultsDate(laboratoryResults)

        Properties nknTableProperties = new Properties()
        File propertiesFile = new File(nknTablePropertiesPath)
        if (propertiesFile.exists()) {
            nknTableProperties.load(new InputStreamReader(new FileInputStream(propertiesFile), "UTF-8"))
        }

        List<LabMeasurement> labResults = laboratoryResults.results.collect { result ->

            String value = extractValue(result)
            String valueMetadata = extractValueMetadata(result)
            def (Analysis analysis, String analysisCode, String analysisType) = extractAnalysis(result, nknTableProperties)
            String analysisTypeMetadata = extractAnalysisMetadata(analysis)
            String requisitionGroup = analysis.requisitionGroup ? analysis.requisitionGroup.name : '-'
            int order = (analysis.order as String)?.isInteger() ? (analysis.order as String).toInteger() : 0
            String unit = extractUnit(result, nknTableProperties)
            def (Indicator indicator, Validation validation) = extractValidationAndIndicator(result)
            boolean hasBeenCorrected = extractHasBeenCorrected(result)
            boolean hasComments = extractHasComments(result)

            new LabMeasurement(value, valueMetadata, analysisType, analysisCode, analysisTypeMetadata,
                    unit, date, indicator, requisitionGroup, validation, order, hasBeenCorrected, hasComments)
        }

        labResults
    }

    private String extractValue(Result result) {
        String value = result.value
        if (value.indexOf('<a href') > 0) {
            value = value.substring(0, value.indexOf('<a href')).trim()
        }

        if (value.length() > 10) {
            value = value.substring(0, 9) + "..."
        }

        if (result.operator && ResultType.fromString(result.resultType as String) == ResultType.Numeric) {
            if (result.operator.equals('mindre_end')) {
                value = "< ${value}"
            }

            if (result.operator.equals('stoerre_end')) {
                value = "> ${value}"
            }
        }

        value
    }

    private String extractValueMetadata(Result result) {
        String valueMetadata = ""

        if (result.resultTextValueOrComment.size() > 0) {
            valueMetadata += "<b>Værditekster og kommentarer:</b>"
            result.resultTextValueOrComment.each { comment ->
                valueMetadata += "<br/>- ${comment}"
            }
        }

        if (result.referenceInterval) {
            ReferenceInterval referenceInterval = result.referenceInterval
            if (valueMetadata.length() > 0) {
                valueMetadata += "<br/>"
            }
            valueMetadata += "<b>Referenceinterval:</b>"

            def lowerLimit = referenceInterval.lowerLimit
            if (lowerLimit) {
                valueMetadata += "<br/>- <b>Nedre grænse</b>: ${lowerLimit}"
            }

            def upperLimit = referenceInterval.upperLimit
            if (upperLimit) {
                valueMetadata += "<br/>- <b>Øvre grænse</b>: ${upperLimit}"
            }

            def intervalText = referenceInterval.intervalText
            if (intervalText) {
                valueMetadata += "<br/>- <b>Interval tekst</b>: ${intervalText}"
            }
        }

        String validationText = UNSPECIFIED
        Validation validation = Validation.fromString(result.resultValidation as String)
        if (validation) {
            switch (validation) {
                case Validation.TooHigh:
                    validationText = 'for høj'
                    break;
                case Validation.TooLow:
                    validationText = 'for lav'
                    break;
                case Validation.Abnormal:
                    validationText = 'Unormal'
                    break;
            }
        }
        if (validationText != UNSPECIFIED) {
            if (valueMetadata.length() > 0) {
                valueMetadata += "<br/>"
            }
            valueMetadata += "<b>Validering:</b> ${validationText}"
        }

        String producer = extractProducer(result)
        if (producer != UNSPECIFIED) {
            if (valueMetadata.length() > 0) {
                valueMetadata += "<br/>"
            }
            valueMetadata += "<b>Producent:</b> ${producer}"
        }

        valueMetadata
    }

    private String extractProducer(Result result) {
        String producer = ""

        if (result.producerOfLabResult) {
            ProducerOfLabResult producerOfLabResult = result.producerOfLabResult
            producer = (producerOfLabResult.identifier != null && producerOfLabResult.identifier != '_') ?
                    producerOfLabResult.identifier : UNSPECIFIED
        }

        producer
    }

    private List extractAnalysis(Result result, Properties nknTableProperties) {
        def analysis = result.analysis
        String analysisCode = analysis.analysisCode

        if (nknTableProperties.getProperty(analysisCode + ".shortName")) {
            analysis.analysisShortName = nknTableProperties.getProperty(analysisCode + ".shortName")
        }

        if (nknTableProperties.getProperty(analysisCode + ".fullName")) {
            analysis.analysisCompleteName = nknTableProperties.getProperty(analysisCode + ".fullName")
        }

        String analysisType = analysis.analysisShortName ?: analysis.analysisCompleteName

        [analysis, analysisCode, analysisType]
    }

    private String extractAnalysisMetadata(Analysis analysis) {
        String analysisCode = analysis.analysisCode
        String analysisCompleteName = analysis.analysisCompleteName

        String analysisTypeMetadata = """<b>Analysenavn:</b> ${analysisCompleteName}
                <br/><b>Analysekode:</b> ${analysisCode}"""

        analysisTypeMetadata
    }

    private String extractUnit(Result result, Properties nknTableProperties) {
        def unit = result.unit
        def analysis = result.analysis
        String analysisCode = analysis.analysisCode

        if (nknTableProperties.getProperty(analysisCode + ".unit")) {
            unit = nknTableProperties.getProperty(analysisCode + ".unit")
        }

        unit
    }

    private Date parseResultsDate(LaboratoryResults laboratoryResults) {
        def resultsDate = laboratoryResults.generalResultInformation.resultsDate
        def resultsTime = laboratoryResults.generalResultInformation.resultsTime
        Date resultDate = DATE_FORMAT.parse("${resultsDate} ${resultsTime}")
        resultDate
    }

    @SuppressWarnings("GroovyAssignabilityCheck")
    private List extractValidationAndIndicator(Result result) {
        Validation validation = Validation.fromString(result.resultValidation as String)
        Indicator indicator

        if (validation) {
            indicator = (validation == Validation.TooHigh) ?
                    Indicator.Above :
                    Indicator.Below
        } else {
            (indicator, validation) = parseReferenceInterval(result)
        }

        [indicator, validation]
    }

    private boolean extractHasBeenCorrected(Result result) {
        result.resultStatusCode != null && result.resultStatusCode.equals('svar_rettet')
    }

    private boolean extractHasComments(Result result) {
        result.resultTextValueOrComment != null && result.resultTextValueOrComment.size() > 0
    }

    private def parseReferenceInterval(Result result) {
        Closure stringToFloat = { input ->
            NumberFormatUtil.parse(makeNumeric(input)).floatValue()
        }

        ResultType resultType = ResultType.fromString(result.resultType as String)
        Indicator resultIndicator = Indicator.None
        Validation validation = null
        def referenceInterval = result.referenceInterval

        if (resultType == ResultType.Numeric && referenceInterval != null) {
            Float lowerLimit = referenceInterval.lowerLimit ?
                    stringToFloat(referenceInterval.lowerLimit) :
                    null
            Float upperLimit = referenceInterval.upperLimit ?
                    stringToFloat(referenceInterval.upperLimit) :
                    null
            Float numericResultValue = result.value ?
                    stringToFloat(result.value) :
                    null

            if (upperLimit && numericResultValue > upperLimit) {
                resultIndicator = Indicator.Above
                validation = Validation.TooHigh
            } else if (lowerLimit && numericResultValue < lowerLimit) {
                resultIndicator = Indicator.Below
                validation = Validation.TooLow
            }
        }

        if (resultType == ResultType.Numeric && referenceInterval == null) {
            resultIndicator = Indicator.Unknown
        }

        [resultIndicator, validation]
    }

    private String makeNumeric(input) {
        def separator = NumberFormatUtil
                .localeSpecificDecimalFormat()
                .decimalFormatSymbols
                .decimalSeparator as String
        return (input as String)
                .replaceAll(/\./, separator)
                .replaceAll(/,/, separator)
                .replaceAll("[^\\d${separator}]", "")
    }

    private List<MeasurementGroup> arrangeByMeasurementGroup(List<LabMeasurement> labMeasurements) {
        Closure groupByRequisitionGroup = { measurements ->
            measurements.groupBy { LabMeasurement result -> result.requisitionGroup() }
        }

        Closure sortByMeasurementOrder = { List<MeasurementGroup> measurementGroups ->
            measurementGroups.each { measurementGroup ->
                measurementGroup.analysisGroups().sort { analysisCodeGroup ->
                    analysisCodeGroup.measurements[0].order()
                }
            }
        }

        Map<String, List> groupNameToMeasurementsMap = groupByRequisitionGroup(labMeasurements)
        List<MeasurementGroup> measurementGroups = assignToMeasurementGroups(groupNameToMeasurementsMap)
        sortByMeasurementOrder(measurementGroups)
        measurementGroups
    }

    private List<MeasurementGroup> assignToMeasurementGroups(Map<String, List> groupNameToMeasurementsMap) {
        Closure groupByAnalysisCode = { measurements ->
            measurements.groupBy { LabMeasurement result -> result.analysisCode() }
        }

        List<MeasurementGroup> measurementGroups = []
        groupNameToMeasurementsMap.keySet().each { String groupName ->
            List<LabMeasurement> groupMeasurements = groupNameToMeasurementsMap[groupName]
            Map<String, List> analysisCodeToMeasurementsMap = groupByAnalysisCode(groupMeasurements)

            List<AnalysisGroup> analysisCodeGroups = []
            analysisCodeToMeasurementsMap.keySet().each { analysisCode ->
                List<LabMeasurement> analysisCodeMeasurements = analysisCodeToMeasurementsMap[analysisCode]
                LabMeasurement analysisMeasurement = analysisCodeMeasurements[0]
                String analysisType = analysisMeasurement.analysisType()
                String analysisTypeMetadata = analysisMeasurement.analysisTypeMetadata()
                String unit = analysisMeasurement.unit()
                analysisCodeGroups += new AnalysisGroup(analysisType, analysisCode,
                        analysisTypeMetadata, unit, analysisCodeMeasurements)
            }

            measurementGroups += new MeasurementGroup(groupName, analysisCodeGroups)
        }

        measurementGroups
    }

}