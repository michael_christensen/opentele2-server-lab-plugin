package org.opentele.server.lab.pogo

import groovy.transform.ToString

@ToString
class GeneralResultInformation {

    def laboratoryInternalProductionIdentifier

    def reportStatusCode

    def resultsDate
    def resultsTime
}
