package org.opentele.server.lab.pogo

import groovy.transform.ToString

@ToString
class ResultReference {

    // Type
    def refDescription

    // One of:
    def url
    def sup
    Bin bin
}
