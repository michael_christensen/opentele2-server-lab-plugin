package org.opentele.server.lab.pogo

import groovy.transform.ToString

@ToString
class RequisitionInformation {

    def comments
    def laboratoryInternalSampleIdentifier
    def requesterSampleIdentifier
    def samplingDate
    def samplingTime

    def volume
    def unitOfVolume
    def samplingIntervalStartDate
    def samplingIntervalStartTime
    def samplingIntervalEndDate
    def samplingIntervalEndTime
}
