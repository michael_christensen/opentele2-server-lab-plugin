package org.opentele.server.lab.pogo

import groovy.transform.ToString

@ToString
class ProducerOfLabResult {

    def identifier
    def identifierCode
}
