package org.opentele.server.lab.pogo

import groovy.transform.ToString

@ToString
class AnalysisDetails {

    String code
    String fullName
    String shortName
    String unit

}
