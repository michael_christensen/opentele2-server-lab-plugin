package org.opentele.server.lab.pogo

import groovy.transform.ToString

@ToString
class Letter {

    def identifier
    def versionCode
    def statisticalCode
    def authorisationDate
    def authorisationTime
    def typeCode

}
