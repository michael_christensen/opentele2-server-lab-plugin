package org.opentele.server.lab.pogo

import groovy.transform.ToString

@ToString
class Result {

    def resultStatusCode
    def value
    def unit
    def resultType
    def operator
    def resultValidation
    def toLabIdentifier

    // max 20
    List<String> resultTextValueOrComment = new ArrayList<String>()

    // max 10
    List<ResultReference> references = new ArrayList<ResultReference>()

    Analysis analysis = new Analysis()

    ProducerOfLabResult producerOfLabResult = new ProducerOfLabResult()
    ReferenceInterval referenceInterval
}
