package org.opentele.server.lab

import groovy.util.slurpersupport.GPathResult
import org.opentele.server.lab.pogo.Bin
import org.opentele.server.lab.pogo.CCReceiver
import org.opentele.server.lab.pogo.Emessage
import org.opentele.server.lab.pogo.GeneralResultInformation
import org.opentele.server.lab.pogo.LabPatient
import org.opentele.server.lab.pogo.LaboratoryReport
import org.opentele.server.lab.pogo.Letter
import org.opentele.server.lab.pogo.Receiver
import org.opentele.server.lab.pogo.ReferenceInterval
import org.opentele.server.lab.pogo.RequisitionGroup
import org.opentele.server.lab.pogo.RequisitionInformation
import org.opentele.server.lab.pogo.Result
import org.opentele.server.lab.pogo.ResultReference
import org.opentele.server.lab.pogo.Sender

class Xrpt01ParserService {

    // --*-- Fields --*--

    def grailsApplication

    private static final String MEDCOM_NAMESPACE = 'http://rep.oio.dk/sundcom.dk/medcom.dk/xml/schemas/2007/02/01/'

    private static final String UTF_8 = "UTF-8"
    private static final String ISO_8859_1 = "ISO-8859-1"

    // XML Elements
    private static final String RESULT_TEXT_VALUE = 'ResultTextValue'
    private static final String REFERENCE_INTERVAL = 'ReferenceInterval'
    private static final String CCRECEIVER = 'CCReceiver'
    private static final String RESULT = 'Result'
    private static final String IDENTIFIER_CODE = 'IdentifierCode'
    private static final String PHYSICIAN = 'Physician'
    private static final String CIVIL_REGISTRATION_NUMBER = 'CivilRegistrationNumber'
    private static final String CONSENT = 'Consent'
    private static final String RESULT_COMMENTS = 'ResultComments'
    private static final String REFERENCE = 'Reference'
    private static final String SAMPLING_INTERVAL = 'SamplingInterval'
    private static final String REQUISITION_GROUP = 'RequisitionGroup'

    List npuCodes

    // --*-- Methods --*--

    Emessage parse(File xmlFile, List npuCodes) {
        GPathResult xrpt01 = new XmlSlurper().parse(xmlFile).declareNamespace([meta:MEDCOM_NAMESPACE])
        parse(xrpt01, npuCodes)
    }

    Emessage parse(String xml, List npuCodes) {
        int encodingOffset = 10
        int indexOfFirstQuote = xml.indexOf("encoding=\"") + encodingOffset
        int indexOfSecondQuote = xml.indexOf("\"", indexOfFirstQuote)

        // Ignore this block when testing
        String encoding = xml.substring(indexOfFirstQuote, indexOfSecondQuote)
        if (encoding == ISO_8859_1) {
            xml = new String(xml.trim().bytes, ISO_8859_1).getBytes(UTF_8)
        }

        GPathResult xrpt01 = new XmlSlurper().parseText(xml).declareNamespace([meta: MEDCOM_NAMESPACE])
        parse(xrpt01, npuCodes)
    }

    Emessage parse(GPathResult xrpt01, List npuCodes) {
        this.npuCodes = npuCodes

        Emessage eMessage = new Emessage()

        eMessage.sentDate = xrpt01.Envelope.Sent.Date.text()
        eMessage.sentTime = xrpt01.Envelope.Sent.Time.text()
        eMessage.identifier = xrpt01.Envelope.Identifier.text()
        eMessage.acknowledgementCode = xrpt01.Envelope.AcknowledgementCode.text()

        xrpt01.LaboratoryReport.each { laboratoryReportNode ->
            def laboratoryReport = new LaboratoryReport()
            parseLetter(laboratoryReportNode.Letter, laboratoryReport.letter)
            parseSender(laboratoryReportNode.Sender, laboratoryReport.sender)
            parseReceiver(laboratoryReportNode.Receiver, laboratoryReport.receiver)

            if (laboratoryReportNode.children().findAll({it.name() == CCRECEIVER}).size() > 0) {
                laboratoryReport.ccReceiver = parseCCReceiver(laboratoryReportNode.CCReceiver)
            }

            parsePatient(laboratoryReportNode.Patient, laboratoryReport.patient)
            parseRequisitionInformation(laboratoryReportNode.RequisitionInformation,
                    laboratoryReport.requisitionInformation)
            parseGeneralResultInformation(laboratoryReportNode.LaboratoryResults.GeneralResultInformation,
                    laboratoryReport.laboratoryResults.generalResultInformation)

            def results = laboratoryReportNode.LaboratoryResults.children().findAll( {it.name() == RESULT})
            results.each { resultXml ->
                def result = parseResult(resultXml)
                if (result) {
                    laboratoryReport.laboratoryResults.results.add(result)
                }
            }

            eMessage.laboratoryReports << laboratoryReport
        }
        eMessage
    }

    def parseLetter(def letterNode, Letter letter) {
        letter.identifier = letterNode.Identifier.text()
        letter.versionCode = letterNode.VersionCode.text()
        letter.statisticalCode = letterNode.StatisticalCode.text()
        letter.authorisationDate = letterNode.Authorisation.Date.text()
        letter.authorisationTime = letterNode.Authorisation.Time.text()
        letter.typeCode = letterNode.TypeCode.text()
    }

    def parseSender(def senderNode, Sender sender) {
        sender.eanIdentifier = senderNode.EANIdentifier.text()
        sender.identifier = senderNode.Identifier.text()

        if (senderNode.IdentifierCode.name() == IDENTIFIER_CODE) {
            sender.identifierCodeOrLocal = senderNode.IdentifierCode.text()
        } else {
            sender.identifierCodeOrLocal = senderNode.IdentifierLocal.text()
        }

        sender.organisationName = senderNode.OrganisationName.text()
        sender.departmentName = handleNonMandatoryNode(senderNode.DepartmentName)
        sender.unitName = handleNonMandatoryNode(senderNode.UnitName)
        sender.medicalSpecialityCode = handleNonMandatoryNode(senderNode.MedicalSpecialityCode)
        sender.fromLabIdentifier = handleNonMandatoryNode(senderNode.FromLabIdentifier)
    }

    def parseReceiver(def receiverNode, Receiver receiver) {
        receiver.eanIdentifier = receiverNode.EANIdentifier.text()
        receiver.identifier = receiverNode.Identifier.text()

        if (receiverNode.IdentifierCode.name() == IDENTIFIER_CODE) {
            receiver.identifierCodeOrLocal = receiverNode.IdentifierCode.text()
        } else {
            receiver.identifierCodeOrLocal = receiverNode.IdentifierLocal.text()
        }

        receiver.organisationName = handleNonMandatoryNode(receiverNode.OrganisationName)
        receiver.departmentName = handleNonMandatoryNode(receiverNode.DepartmentName)
        receiver.unitName = handleNonMandatoryNode(receiverNode.UnitName)
        receiver.streetName = handleNonMandatoryNode(receiverNode.StreetName)
        receiver.suburbName = handleNonMandatoryNode(receiverNode.SuburbName)
        receiver.districtName = handleNonMandatoryNode(receiverNode.DistrictName)
        receiver.postCodeIdentifier = handleNonMandatoryNode(receiverNode.PostCodeIdentifier)

        // Not mandatory
        if (receiverNode.children().findAll({it.name() == PHYSICIAN }).size() > 0) {
            receiver.physicianInitials = receiverNode.Physician.PersonInitials.text()
        }
    }

    CCReceiver parseCCReceiver(def ccReceiverNode) {

        CCReceiver ccReceiver = new CCReceiver() // Not mandatory

        ccReceiver.identifier = ccReceiverNode.Identifier.text()

        if (ccReceiverNode.IdentifierCode.name() == IDENTIFIER_CODE) {
            ccReceiver.identifierCodeOrLocal = ccReceiverNode.IdentifierCode.text()
        } else {
            ccReceiver.identifierCodeOrLocal = ccReceiverNode.IdentifierLocal.text()
        }

        ccReceiver.organisationName = handleNonMandatoryNode(ccReceiverNode.OrganisationName)
        ccReceiver.departmentName = handleNonMandatoryNode(ccReceiverNode.DepartmentName)
        ccReceiver.unitName = handleNonMandatoryNode(ccReceiverNode.UnitName)

        ccReceiver
    }

    def parsePatient(def patientNode, LabPatient patient) {

        if (patientNode.CivilRegistrationNumber.name() == CIVIL_REGISTRATION_NUMBER) {
            patient.cpr = patientNode.CivilRegistrationNumber.text()
        } else {
            patient.cpr = patientNode.AlternativeIdentifier.text()
        }

        patient.surname = patientNode.PersonSurnameName.text()
        patient.givenName = handleNonMandatoryNode(patientNode.PersonGivenName)

        if (patientNode.children().findAll({it.name() == CONSENT }).size() > 0) {
            patient.consentGiven = patientNode.Consent.Given.text()
        }
    }

    def parseGeneralResultInformation(def generalResultInformationNode,
                                      GeneralResultInformation generalResultInformation) {
        generalResultInformation.laboratoryInternalProductionIdentifier =
                generalResultInformationNode.LaboratoryInternalProductionIdentifier.text()
        generalResultInformation.reportStatusCode = generalResultInformationNode.ReportStatusCode.text()
        generalResultInformation.resultsDate = generalResultInformationNode.ResultsDateTime.Date.text()
        generalResultInformation.resultsTime = generalResultInformationNode.ResultsDateTime.Time.text()
    }

    void parseRequisitionInformation(def requisitionInformationNode,
                                     RequisitionInformation requisitionInformation) {

        requisitionInformation.comments = requisitionInformationNode.Comments.text()
        requisitionInformation.laboratoryInternalSampleIdentifier =
                requisitionInformationNode.Sample.LaboratoryInternalSampleIdentifier.text()
        requisitionInformation.requesterSampleIdentifier =
                handleNonMandatoryNode(requisitionInformationNode.Sample.RequesterSampleIdentifier)
        requisitionInformation.samplingDate =
                requisitionInformationNode.Sample.SamplingDateTime.Date.text()
        requisitionInformation.samplingTime =
                requisitionInformationNode.Sample.SamplingDateTime.Time.text()
        requisitionInformation.volume =
                handleNonMandatoryNode(requisitionInformationNode.Sample.Volume)
        requisitionInformation.unitOfVolume =
                handleNonMandatoryNode(requisitionInformationNode.Sample.UnitOfVolume)

        if (requisitionInformationNode.Sample.children()
                .findAll({it.name() == SAMPLING_INTERVAL}).size() > 0) {
            requisitionInformation.samplingIntervalStartDate =
                    requisitionInformationNode.Sample.SamplingInterval.StartDateTime.Date.text()
            requisitionInformation.samplingIntervalStartTime =
                    requisitionInformationNode.Sample.SamplingInterval.StartDateTime.Time.text()
            requisitionInformation.samplingIntervalEndDate =
                    requisitionInformationNode.Sample.SamplingInterval.EndDateTime.Date.text()
            requisitionInformation.samplingIntervalEndTime =
                    requisitionInformationNode.Sample.SamplingInterval.EndDateTime.Time.text()
        }
    }

    Result parseResult(def resultNode) {

        Result result = new Result()

        result.resultStatusCode = resultNode.ResultStatusCode.text()
        result.resultType = resultNode.ResultType.text()
        result.operator = handleNonMandatoryNode(resultNode.Operator)
        result.value = resultNode.Value.text()
        result.unit = handleNonMandatoryNode(resultNode.Unit)
        result.resultValidation = handleNonMandatoryNode(resultNode.ResultValidation)

        parseAnalysis(resultNode.Analysis, result)

        // Ignore this block when testing
        if (!(result.analysis.analysisCode in npuCodes)) {
            return null
        }

        result.producerOfLabResult.identifier = resultNode.ProducerOfLabResult.Identifier.text()
        result.producerOfLabResult.identifierCode = resultNode.ProducerOfLabResult.IdentifierCode.text()

        if (resultNode.children().findAll({it.name() == REFERENCE_INTERVAL}).size() > 0) {
            def referenceInterval = resultNode.ReferenceInterval

            if (referenceInterval) {
                result.referenceInterval = new ReferenceInterval()
                result.referenceInterval.typeOfInterval =
                        referenceInterval.TypeOfInterval.text()
                result.referenceInterval.lowerLimit =
                        handleNonMandatoryNode(referenceInterval.LowerLimit)
                result.referenceInterval.upperLimit =
                        handleNonMandatoryNode(referenceInterval.UpperLimit)
                result.referenceInterval.intervalText =
                        handleNonMandatoryNode(referenceInterval.IntervalText)
            }
        }

        def resultTexts = resultNode.children().findAll( {
            (it.name() == RESULT_TEXT_VALUE || it.name() == RESULT_COMMENTS)
        })

        resultTexts.each { txt ->
            String text = txt.text()
            if (text != null && text.length() > 0) {
                result.resultTextValueOrComment.add(text)
            }
        }

        def references =  resultNode.children().findAll( {it.name() == REFERENCE})
        references.each { refNode ->
            result.references.add(parseReference(refNode))
        }

        result.toLabIdentifier = handleNonMandatoryNode(resultNode.ToLabIdentifier)

        result
    }

    void parseAnalysis(def analysisNode, Result result) {
        result.analysis.analysisCode = analysisNode.AnalysisCode.text()
        result.analysis.analysisCodeType = analysisNode.AnalysisCodeType.text()
        result.analysis.analysisCodeResponsible = analysisNode.AnalysisCodeResponsible.text()
        result.analysis.analysisShortName = handleNonMandatoryNode(analysisNode.AnalysisShortName)
        result.analysis.analysisCompleteName = analysisNode.AnalysisCompleteName.text()
        result.analysis.order = analysisNode.Order.text()

        if (analysisNode.children().findAll({it.name() == REQUISITION_GROUP }).size() > 0) {
            result.analysis.requisitionGroup = new RequisitionGroup()

            result.analysis.requisitionGroup.identifier =
                    analysisNode.RequisitionGroup.Identifier.text()
            result.analysis.requisitionGroup.identifierResponsible =
                    analysisNode.RequisitionGroup.IdentifierResponsible.text()
            result.analysis.requisitionGroup.name =
                    analysisNode.RequisitionGroup.Name.text()
        }
    }

    ResultReference parseReference(def referenceNode) {
        ResultReference ref = new ResultReference()

        ref.refDescription = referenceNode.RefDescription.text()
        ref.url = handleNonMandatoryNode(referenceNode.URL)
        ref.sup = handleNonMandatoryNode(referenceNode.SUP)

        if (!referenceNode.bin.isEmpty()) {

            ref.bin = new Bin()
            ref.bin.objectIdentifier = referenceNode.BIN.ObjectIdentifier.text()
            ref.bin.objectCode = referenceNode.BIN.ObjectCode.text()
            ref.bin.objectExtensionCode = referenceNode.BIN.ObjectExtensionCode.text()
            ref.bin.originalObjectSize = referenceNode.BIN.OriginalObjectSize.text()
        }
        ref
    }

    def handleNonMandatoryNode(def node) {
        node.isEmpty() ? null : node.text()
    }
}