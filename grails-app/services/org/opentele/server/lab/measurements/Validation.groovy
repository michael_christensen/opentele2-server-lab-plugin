package org.opentele.server.lab.measurements

public enum Validation {

    // --*-- Enums --*--

    TooLow('for_lav'),
    TooHigh('for_hoej'),
    Abnormal('unormal')

    // --*-- Fields --*--

    String parsedValue

    // --*-- Constructors --*--

    Validation(String parsedValue) {
        this.parsedValue = parsedValue
    }

    // --*-- Methods --*--

    static Validation fromString(String parsedValue) {
        if (TooLow.parsedValue.equals(parsedValue)) {
            return TooLow
        }
        if (TooHigh.parsedValue.equals(parsedValue)) {
            return TooHigh
        }
        if (TooHigh.parsedValue.equals(parsedValue)) {
            return Abnormal
        }
        return null
    }

    @SuppressWarnings("GroovyUnusedDeclaration")
    boolean isTooHigh() {
        parsedValue == 'for_hoej'
    }

    @SuppressWarnings("GroovyUnusedDeclaration")
    boolean isTooLow() {
        parsedValue == 'for_lav'
    }

    @SuppressWarnings("GroovyUnusedDeclaration")
    boolean isAbnormal() {
        parsedValue == 'unormal'
    }

}