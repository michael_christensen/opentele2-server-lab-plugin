package org.opentele.server.lab.measurements

public enum ResultType {

    // --*-- Enums --*--

    Numeric('numerisk'),
    Alphanumeric('alfanumerisk')

    // --*-- Fields --*--

    String parsedValue

    // --*-- Constructors --*--

    ResultType(String parsedValue) {
        this.parsedValue = parsedValue
    }

    // --*-- Methods --*--

    static ResultType fromString(String parsedValue) {
        if (Numeric.parsedValue.equals(parsedValue)) {
            return Numeric
        }
        if (Alphanumeric.parsedValue.equals(parsedValue)) {
            return Alphanumeric
        }
        return null
    }

}