package org.opentele.server.lab

import org.opentele.server.lab.pogo.Emessage
import org.opentele.server.model.Patient
import org.springframework.web.context.request.RequestContextHolder

import java.util.concurrent.TimeUnit

import net.jodah.expiringmap.*

class LabResultCachingService {

    // --*-- Fields --*--

    def grailsApplication

    // --*-- Methods --*--

    List<Emessage> getCachedResult(Patient patient) {

        def session = RequestContextHolder.currentRequestAttributes().getSession()

        Map<Long, List<Emessage>> cachedLabResults = session.cachedLabResults
        if (!cachedLabResults) {
            return []
        }

        List<Emessage> cachedLabResult = cachedLabResults[patient.id]
        if (!cachedLabResult) {
            return []
        }

        cachedLabResult
    }

    void setCachedResult(Patient patient, List<Emessage> labResult) {
        def session = RequestContextHolder.currentRequestAttributes().getSession()

        if (!session.cachedLabResults) {
            int cacheTimeInMinutes = grailsApplication.config.lab.cacheTimeInMinutes
            Map expiringMap = ExpiringMap.builder()
                    .expiration(cacheTimeInMinutes, TimeUnit.MINUTES)
                    .expirationPolicy(ExpirationPolicy.CREATED)
                    .build()
            session.cachedLabResults = expiringMap
        }

        session.cachedLabResults.put(patient.id, labResult)
    }

}
