package org.opentele.server.lab

import grails.plugin.springsecurity.annotation.Secured
import org.opentele.server.core.model.types.PermissionName
import org.opentele.server.lab.measurements.MeasurementGroup
import org.opentele.server.model.Patient
import org.springframework.beans.factory.annotation.Value

import java.text.SimpleDateFormat

@Secured(PermissionName.NONE)
class LabController {

    // --*-- Fields --*--

    private static final String DATE_FORMAT = "yyyy-MM-dd"

    @Value('${lab.intervalInDays:90}')
    int intervalInDays

    def springSecurityService
    def labService

    static allowedMethods = [show: 'GET']

    // --*-- Actions --*--

    @Secured(PermissionName.LAB_READ)
    def show() {
        String patientId = params.id
        def patient = Patient.get(patientId)
        def (from, to) = getFromAndToDates(intervalInDays)

        List<MeasurementGroup> measurementGroups = []
        List<Date> measurementDates = []
        String error = null

        try {
            measurementGroups = labService.getMeasurementGroups(patient)
            measurementDates = getMeasurementDates(measurementGroups)
        } catch (Exception e) {
            e.printStackTrace()
            error = e.message
            if (!error && e.undeclaredThrowable) {
                error = e.undeclaredThrowable.message
            }
            if (!error) {
                error = 'lab.errors.unknown'
            }
        }

        flash.error = error
        [patientInstance: patient, measurementGroups: measurementGroups,
         measurementDates: measurementDates, from: from, to: to]
    }

    private static List getFromAndToDates(int intervalInDays) {
        Date toDate = new Date()
        Calendar fromCalendar = new GregorianCalendar()
        fromCalendar.add(Calendar.DAY_OF_YEAR, -intervalInDays)
        Date fromDate = fromCalendar.getTime()

        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT)
        def from = dateFormat.format(fromDate)
        def to = dateFormat.format(toDate)
        [from, to]
    }

    private static List<Date> getMeasurementDates(List<MeasurementGroup> measurementGroups) {
        measurementGroups.analysisGroups.measurements.flatten().collect({ it.time()}).unique().sort().reverse()
    }

}