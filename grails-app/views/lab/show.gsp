<%@ page import="org.opentele.server.lab.LabService" %>
<%@ page import="org.opentele.server.lab.measurements.Indicator" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout"
          content="main">
    <title>
        <g:message code="lab.show.title"/>
    </title>
    <style>
    td {
        text-align: left !important;
        min-width: 100px;
    }
    th {
        min-width: 100px;
    }
    .note {
        position: relative;
    }
    .note:after {
        content: "";
        position: absolute;
        top: 0;
        right: 0;
        width: 0;
        height: 0;
        display: block;
        border-left: 20px solid transparent;
        border-bottom: 20px solid transparent;

        border-top: 20px solid #f00;
    }
    </style>
</head>
<body>

<div id="lab-content"
     class="content"
     role="main">

    <h1><g:message code="lab.show.measurements" args="[patientInstance.name, from, to]"/></h1>

    <g:if test="${flash.error}">
        <div class="errors" role="status">
            <g:message code="${flash.error}"/>
        </div>
    </g:if>

    <g:if test="${measurementGroups}">
        <ol class="property-list">
            <span class="property-value">

                <g:each var="measurementGroup" in="${measurementGroups}">
                    <h1>${measurementGroup.type()}</h1>

                    <div id="list-patient" class="content scaffold-list" style="overflow-x: scroll;">
                        <table style="table-layout: auto">
                            <thead>
                            <tr>
                                <th><g:message code="lab.analysisType.label"/></th>
                                <th><g:message code="lab.unit.label"/></th>
                                <g:each in="${measurementDates}" status="i" var="measurementDate">
                                    <th data-tooltip="${LabService.DATE_FORMAT.format(measurementDate)}">
                                        <g:formatDate format="dd.MM.yy" date="${measurementDate}"/>
                                    </th>
                                </g:each>
                            </tr>
                            </thead>
                            <tbody>
                            <g:each status="i" var="analysisGroup" in="${measurementGroup.analysisGroups()}">
                                <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                                    <td style="min-width: 200px;"
                                        data-tooltip="${analysisGroup.analysisTypeMetadata}">${analysisGroup.analysisType}</td>
                                    <td>${analysisGroup.unit}</td>

                                    <g:each var="measurementDate" in="${measurementDates}">
                                        <g:set var="foundMeasurement" value="${false}"/>

                                        <g:each var="measurement" in="${analysisGroup.measurements}">
                                            <g:if test="${measurement.time().equals(measurementDate) && !foundMeasurement}">

                                                <g:set var="foundMeasurement" value="${true}"/>
                                                <g:if test="${measurement.valueMetadata().length() > 0}">
                                                    <td class="${measurement.hasComments() ? "note" : ""}"
                                                    data-tooltip="${measurement.valueMetadata()}">
                                                </g:if>
                                                <g:else>
                                                    <td class="${measurement.hasComments() ? "note" : ""}">
                                                </g:else>
                                                    <g:if test="${measurement.validation() &&
                                                            measurement.validation().isTooHigh()}">
                                                        <span style="color: red">${measurement.value()}</span>
                                                    </g:if>
                                                    <g:elseif test="${measurement.validation() &&
                                                            measurement.validation().isTooLow()}">
                                                        <span style="color: blue">${measurement.value()}</span>
                                                    </g:elseif>
                                                    <g:elseif test="${measurement.indicator() == Indicator.Unknown}">
                                                        <span style="color: gray">${measurement.value()}</span>
                                                    </g:elseif>
                                                    <g:else>
                                                        <span>${measurement.value()}</span>
                                                    </g:else>
                                                    <span>${measurement.indicator().unicodeCharacter}
                                                        <g:if test="${measurement.hasBeenCorrected()}"><b>R</b></g:if>
                                                    </span>
                                                </td>

                                            </g:if>
                                        </g:each>

                                        <g:if test="${!foundMeasurement}">
                                            <td></td>
                                        </g:if>

                                    </g:each>

                                </tr>
                            </g:each>
                            </tbody>
                        </table>
                    </div>
                </g:each>
            </span>
        </ol>
    </g:if>
    <g:else>
        <p><g:message code="lab.show.nomeasurements"/></p>
    </g:else>

</div>
</body>
</html>
